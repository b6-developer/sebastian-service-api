package com.adprogb6.sebastian.model.datamodel.cuaca;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import static org.junit.Assert.*;

public class IdToDescriptionTest {

    @Test
    public void testConstructorVisibility() throws NoSuchMethodException {
        Constructor<IdToDescription> constructor = IdToDescription.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }

    @Test
    public void testSunnyCode() {
        assertEquals("cerah", IdToDescription.translateId(800));
    }

    @Test
    public void testUnclearAtmosphereCode() {
        assertEquals("pusaran pasir/debu", IdToDescription.translateId(730));
    }

    @Test
    public void testCloudyCode() {
        assertEquals("berawan", IdToDescription.translateId(830));
    }

    @Test
    public void testOtherCode() {
        assertEquals("badai berpetir", IdToDescription.translateId(230));
    }
}
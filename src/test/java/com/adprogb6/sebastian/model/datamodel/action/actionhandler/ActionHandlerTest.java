package com.adprogb6.sebastian.model.datamodel.action.actionhandler;

import com.adprogb6.sebastian.model.datamodel.action.actionfactory.ActionFactory;
import com.adprogb6.sebastian.model.datamodel.action.actionhandler.Grammar.GrammarAction;
import com.adprogb6.sebastian.service.Inputprocessing.InputService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ActionHandlerTest {
    private ActionFactory actionFactory = new ActionFactory();
    private final String failedResponseGrammar = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";
    private final String failedResponseWeather = "Maaf tuan, saya tidak bisa menemukan cuaca untuk kota tersebut";
    private final String failedResponseTemp = "Maaf tuan, saya tidak bisa menemukan suhu untuk kota tersebut";
    private final String failedResponseHumidity = "Maaf tuan, saya tidak bisa menemukan kelembapan untuk kota tersebut";
    private final String failedResponseMusicChart = "Maaf tuan, saya tidak bisa menemukan chart artis yang anda cari";
    private final String failedResponseSongLyric = "Maaf tuan, saya tidak bisa menemukan lirik lagu yang anda cari";


    @Autowired
    InputService inputService;

    @Test
    public void actionFactoryCreateCorrectObject() {
        String input = "Sebastian, cari sinonim mampus";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertTrue(actionHandler instanceof GrammarAction);
    }

    @Test
    public void actionHandlerCanHandleValidInput() {
        String input = "Sebastian, cari sinonim mampus";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertNotEquals(failedResponseGrammar, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerReturnFailedResponseWithInvalidInput() {
        String input = "Sebastian, cari sinonim fight";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(failedResponseGrammar, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerGrammarCanReturnAntonymResponse() {
        String input = "Sebastian, cari antonim rusuh";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertNotEquals(failedResponseGrammar, actionHandler.getTextMessageContent());
        assertTrue(actionHandler.getTextMessageContent().contains("antonim"));
    }

    @Test
    public void actionHandlerWordDescriptionCanReturnValidResponse() {
        String input = "Sebastian, cari makna belajar";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertNotEquals(failedResponseGrammar, actionHandler.getTextMessageContent());
        assertTrue(actionHandler.getTextMessageContent().contains("makna"));
    }

    @Test
    public void actionHandlerWordDescriptionReturnFailedResponseWithInvalidInput() {
        String input = "Sebastian, cari makna ancur";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(failedResponseGrammar, actionHandler.getTextMessageContent());
        assertFalse(actionHandler.getTextMessageContent().contains("makna"));
    }

    @Test
    public void actionHandlerStandardWordCanReturnValidInput() {
        String input = "Sebastian, cari kebakuan belajar";
        String validResponse = "Baik tuan, belajar adalah kata baku";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(validResponse, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerStandardWordCanReturnInvalidInput() {
        String input = "Sebastian, cari kebakuan ancur";
        String validResponse = "Baik tuan, ancur bukanlah kata baku";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(validResponse, actionHandler.getTextMessageContent());
    }
    
    @Test
    public void actionHandlerWeatherCanHandleValidInput() {
        String input = "Sebastian, tolong laporkan-cuaca depok";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertNotEquals(failedResponseWeather, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerWeatherReturnFailedResponseWithInvalidInput() {
        String input = "Sebastian, tolong laporkan-cuaca asdfjsdfhljah";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(failedResponseWeather, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerTempCanHandleValidInput() {
        String input = "Sebastian, tolong laporkan-suhu depok";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertNotEquals(failedResponseTemp, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerTempReturnFailedResponseWithInvalidInput() {
        String input = "Sebastian, tolong laporkan-suhu asdfjsdfhljah";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(failedResponseTemp, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerHumidityCanHandleValidInput() {
        String input = "Sebastian, tolong laporkan-kelembapan depok";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertNotEquals(failedResponseHumidity, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerHumidityReturnFailedResponseWithInvalidInput() {
        String input = "Sebastian, tolong laporkan-kelembapan asdfjsdfhljah";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(failedResponseHumidity, actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHanlderButlerCanChooseFromUserInputNormal() {
        String input = "Sebastian, tolong pilih belajar atau candu";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        String[] commandParameter = inputService.getContent();
        assertNotNull(actionHandler.getTextMessageContent());
        boolean isInContentParamater = false;

       for(String eachParameter : inputService.getContent()) {
           if (actionHandler.getTextMessageContent().contains(eachParameter)) {
               isInContentParamater = true;
           }
       }

       assertTrue(isInContentParamater);
    }

    @Test
    public void  actionHanlderButlerCanChooseFromUserInputMultiple() {
        String input = "Sebastian, tolong pilih belajar atau candu atau tidur atau tertawa";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertNotNull(actionHandler.getTextMessageContent());
        boolean isInContentParamater = false;

        for(String eachParameter : inputService.getContent()) {
            if (actionHandler.getTextMessageContent().contains(eachParameter)) {
                isInContentParamater = true;
            }
        }
        assertTrue(isInContentParamater);
    }

    @Test
    public void actionHandlerButlerCanChooseFromUserInputWithoutBridgeKeyWords() {
        String input = "Sebastian, tolong pilih aku full combo ringing bloom expert";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals("Saya memilih aku full combo ringing bloom expert tuan", actionHandler.getTextMessageContent());
    }

    @Test
    public void actionHandlerButlerCanReturnNumberWithinRange() {
        String input = "Sebastian, tolong roll 200";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertTrue(actionHandler.getTextMessageContent().contains("Baik tuan, saya mendapatkan "));
    }

    @Test
    public void actionHandlerButlerCanReturnRandomYesNo() {
        String input = "Sebastian, tolong tentukan apakah hari menurutmu dingin?";
        inputService.takeInput(input);
        ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
       assertTrue(actionHandler.getTextMessageContent().contains("apakah hari menurutmu dingin?"));
    }


    @Test
    public void actionHandlerHelperCanReturnTotalHelpInfo() {
        String expected = "Baik tuan, perintah yang tersedia adalah: \n" +
                " \n" +
                "Bantuan  tata bahasa:    cari makna, cari kebakuan, cari sinonim, cari antonim \n" +
                " \n" +
                "Bantuan butler: tolong pilih, tolong tentukan, tolong kembalikan, tolong laporkan-cuaca, tolong laporkan-suhu, tolong laporkan-kelembapan\n" +
                " \n" +
                "Bantuan musik: berikan top-artist [jumlah artist], berikan top-songs [jumlah lagu], berikan informasi, berikan lirik-lagu, berikan artis\n" +
                " \n" +
                "Bantuan berita: tampilkan top-headlines, tampilkan berita,tampilkan kategori \n" +
                " \n" +
                "Secara umum perintah yang bisa tuan lakukan adalah Sebastian, [identifier kelompok fitur] [perintah spesifik] [parameter]";

        String input = "Sebastian, help command all";
        inputService.takeInput(input);
        ActionHandler actionHandler = actionFactory.createAction(inputService.getIdentifier(),
                inputService.getCommand(), inputService.getContent());
        assertEquals(expected, actionHandler.getTextMessageContent());
    }
}
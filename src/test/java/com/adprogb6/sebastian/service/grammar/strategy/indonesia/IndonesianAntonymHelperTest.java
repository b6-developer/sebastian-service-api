package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import com.adprogb6.sebastian.service.grammar.strategy.AntonymHelper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class IndonesianAntonymHelperTest {
    private AntonymHelper antonymHelper = new IndonesianAntonymHelper();
    private final String failedReponse = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";

    @Test
    public void antonymHelperCanReturnSuccessResponseWithValidInput() {
        String input = "Sedih";
        assertNotEquals(failedReponse, antonymHelper.findAntonym(input));
    }

    @Test
    public void antonymHelperReturnFailedResponseWithInvalidInput() {
        String input = "Sadness";
        assertEquals(failedReponse, antonymHelper.findAntonym(input));
    }

    @Test
    public  void  antonymHelperCanReturnSuccessResponseWithUnStemmedInput() {
        String input = "menyedihkan";
        assertNotEquals(failedReponse, antonymHelper.findAntonym(input));
    }

    @Test
    public void antonymHelperCanAcceptEmptyString() {
        String input = "";
        assertEquals(failedReponse, antonymHelper.findAntonym(input));
    }

}
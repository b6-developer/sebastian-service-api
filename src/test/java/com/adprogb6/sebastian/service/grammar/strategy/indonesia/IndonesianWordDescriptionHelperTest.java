package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import org.junit.Test;

import static org.junit.Assert.*;

public class IndonesianWordDescriptionHelperTest {
    private IndonesianWordDescriptionHelper wordDescriptionHelper = new IndonesianWordDescriptionHelper();
    private final String failedReponse = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";

    @Test
    public void wordDescriptionHelperCanReturnSuccessResponseWithValidInput() {
        assertNotEquals(failedReponse, wordDescriptionHelper.findMeaning("suara"));
    }

    @Test
    public void wordDescriptionHelperReturnFailedResponseWithInvalidInput() {
        assertEquals(failedReponse, wordDescriptionHelper.findMeaning("knowledge"));
    }
    
    @Test
    public void standardWordReturnValidInput() {
        assertTrue(wordDescriptionHelper.isStandard("makan"));
    }

    @Test
    public void standardWordReturnInvalidInput() {
        assertFalse(wordDescriptionHelper.isStandard("ancur"));
    }

    @Test
    public void wordDescriptionHelperCanAcceptEmptyString() {
        String input = "";
        assertEquals(failedReponse, wordDescriptionHelper.findMeaning(input));
    }
}

package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import org.junit.Test;

import static org.junit.Assert.*;

public class IndonesianSynonymHelperMicroserviceTest {

    private IndonesianSynonymHelperMicroservice indonesianSynonymHelper = new IndonesianSynonymHelperMicroservice();
    private final String failedReponse = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";

    @Test
    public void synonymHelperCanReturnSuccessResponseWithValidInput() {
        String input = "paman";
        assertNotEquals(failedReponse, indonesianSynonymHelper.findSynonym(input));
    }
    @Test
    public void synonymHelperCanReturnSuccessResponseWithUnStemmedInput() {
        String input = "menyuarakan";
        assertNotEquals(failedReponse, indonesianSynonymHelper.findSynonym(input));
    }
    @Test
    public void synonymHelperReturnFailedResponseWithInvalidInput() {
        String input = "sparkling";
        assertEquals(failedReponse, indonesianSynonymHelper.findSynonym(input));
    }

    @Test
    public void synonymHelperCanAcceptEmptyString() {
        String input = "";
        assertEquals(failedReponse, indonesianSynonymHelper.findSynonym(input));
    }

}
package com.adprogb6.sebastian.service.butler;

import org.junit.Test;

import static org.junit.Assert.*;

public class ButlerHelperTest {

    private ButlerHelper butlerHelper = ButlerHelper.getInstance();

    @Test
    public void  butlerCanReturnRandomAnswer() {
        assertNotNull(butlerHelper.getAnswer(new String[]{"Apakah", "biru","?"}));
    }

    @Test
    public void butlerCanReturnNumberWithinRange() {
        int result = butlerHelper.getRandomNumber(100);
        assertTrue(result <= 100);
    }

    @Test
    public void butlerCanChooseChoiceFromInput() {
        String[] input = new String[]{"ancol", "atau", "dufan", "lokom", "kentang"};
        String response = butlerHelper.choose(input);
        assertTrue(response.contains(input[0])||
             response.contains(" dufan lokom kentang"));
    }

}
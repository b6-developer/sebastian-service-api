package com.adprogb6.sebastian.service.Inputprocessing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InputServiceTest {

    @Autowired
    private InputService inputService;

    @Test
    public void serviceAcceptValidInput() {
        String param = "Sebastian, cari makna hidup";
        inputService.takeInput(param);
        assertEquals("Sebastian,",inputService.getHeadInput());
        assertEquals("cari",inputService.getIdentifier());
        assertEquals("makna",inputService.getCommand());
        assertNotNull(inputService.getContent());
        assertTrue(inputService.isValidInput());
    }
    @Test
    public void serviceAcceptMiddleWordValidInput() {
        String param = "Lala hahah lahlaha, ngomong-ngomong sebastian, cari sinonim hidup";
        inputService.takeInput(param);
        assertEquals("sebastian,",inputService.getHeadInput());
        assertEquals("cari",inputService.getIdentifier());
        assertEquals("sinonim",inputService.getCommand());
        assertNotNull(inputService.getContent());
        assertTrue(inputService.isValidInput());
    }

    @Test
    public void serviceDenyInvalidInput() {
        String param = "Sebastian, kerjakan tugas akhir";
        inputService.takeInput(param);
        assertTrue(inputService.isACommand());
        assertFalse(inputService.isValidInput());
    }
    @Test
    public void serviceIgnoreNonCommandInput() {
        String param = "Determination Symphony susah";
        inputService.takeInput(param);
        assertFalse(inputService.isValidInput());

    }

    @Test
    public void serviceCanPrintNonValidCommandResponse() {
        String param = "Sebastian, tertawa sampai mati";
        inputService.takeInput(param);
        assertFalse(inputService.isValidInput());
        assertEquals("maaf, saya tidak bisa memahami yang tuan katakan",inputService.getCommand());
        assertEquals("maaf, saya tidak bisa memahami yang tuan katakan",inputService.getIdentifier());
    }

    @Test
    public void  serviceHandleUnfinishedInput() {
        String param = "Sebastian,";
        inputService.takeInput(param);
        assertFalse(inputService.isValidInput());
    }

    @Test
    public void serviceHandleUnfinishedParameterContent() {
        String param = "Sebastian, cari makna";
        inputService.takeInput(param);
        assertFalse(inputService.isValidInput());
    }
    @Test
    public void serviceCanAcceptHelpCommand() {
        String param = "Sebastian, help all command";
        inputService.takeInput(param);
        assertTrue(inputService.isACommand());
        assertTrue(inputService.isValidInput());
    }


}
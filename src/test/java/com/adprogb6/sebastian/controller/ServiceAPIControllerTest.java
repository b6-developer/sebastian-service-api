package com.adprogb6.sebastian.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashMap;

import static org.junit.Assert.assertNotEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ServiceAPIControllerTest {

    @Autowired
    MockMvc mockMvc;

    private final String BASE_URI = "/api/service-response";
    private final String FAILED_RESPONSE = "maaf, saya tidak bisa memahami yang tuan katakan";


    @Test
    public  void givenInvalidInputShouldReturnCorrectResponse() throws Exception {
        String testString1 = "Sebastian, tolong aaaa";
        String testString2  = "Sebastian, ";
        String testString3 = "Sebastian, wwww";

        String requestString = createRequestBody(testString1);
        validateRequest(requestString, FAILED_RESPONSE);

        requestString = createRequestBody(testString2);
        validateRequest(requestString, FAILED_RESPONSE);

        requestString  = createRequestBody(testString3);
        validateRequest(requestString, FAILED_RESPONSE);
    }

    @Test
    public void  givenAValidInputShouldReturnCorrectResponse() throws Exception {
        String testString1 = "Sebastian, tolong laporkan-cuaca depok";
        String testString2  = "Sebastian, tolong roll 5000 ";
        String testString3 = "Sebastian, cari sinonim bunga";

        String requestString  =  createRequestBody(testString1);
        String result1 = getResponseResult(requestString);
        assertNotEquals(result1, FAILED_RESPONSE);

        requestString  =  createRequestBody(testString2);
        String result2 = getResponseResult(requestString);
        assertNotEquals(result2, FAILED_RESPONSE);

        requestString  =  createRequestBody(testString3);
        String result3 = getResponseResult(requestString);
        assertNotEquals(result3, FAILED_RESPONSE);

    }

    @Test
    public void givenNotAcommandRequestShouldReturnNothing() throws Exception {
        String testString1 = "Diam-diam dia meniru suara kambingku";
        String testString2  = "Lol that was funny";
        String testString3 = "";

        String requestString = createRequestBody(testString1);
        validateRequest(requestString, "");

        requestString = createRequestBody(testString2);
        validateRequest(requestString, "");

        requestString  = createRequestBody(testString3);
        validateRequest(requestString, "");
    }



    private void validateRequest(String requestString, String expectedResponse) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(BASE_URI)
        .contentType(MediaType.APPLICATION_JSON)
        .content(requestString)
        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponse));
    }

    private String getResponseResult(String requestString) throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(BASE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestString)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        return result.getResponse().getContentAsString();
    }

    private String createRequestBody(String requestArgument) throws JsonProcessingException {
        HashMap<String, String> requestBody = new HashMap<>();
        requestBody.put("request", requestArgument);
        ObjectMapper mapper = new ObjectMapper();
        String requestString = mapper.writeValueAsString(requestBody);
        return requestString;
    }

}
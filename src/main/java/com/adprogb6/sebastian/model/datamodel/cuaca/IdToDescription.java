package com.adprogb6.sebastian.model.datamodel.cuaca;

public class IdToDescription {
    private static String[] deskripsi = new String[9];
    private static String[] deskripsiAtmosfer = new String[9];
    private static boolean arraySiap = false;

    private IdToDescription() {
        throw new IllegalStateException("Utility class");
    }

    static void setUpDeskripsi() {
        deskripsi[2] = "badai berpetir";
        deskripsi[3] = "hujan rintik-rintik";
        deskripsi[5] = "hujan";
        deskripsi[6] = "salju";
        deskripsi[8] = "berawan";

        deskripsiAtmosfer[0] = "berkabut";
        deskripsiAtmosfer[1] = "berasap";
        deskripsiAtmosfer[2] = "berkabut tipis";
        deskripsiAtmosfer[3] = "pusaran pasir/debu";
        deskripsiAtmosfer[4] = "berkabut tebal";
        deskripsiAtmosfer[5] = "berdebu";
        deskripsiAtmosfer[6] = "berdebu";
        deskripsiAtmosfer[7] = "berangin";
        deskripsiAtmosfer[8] = "tornado";
        setArraySiap(true);
    }

    static void setArraySiap(boolean value) {
        arraySiap = value;
    }

    static String translateId(int kodeCuaca) {
        if (!arraySiap) {
            setUpDeskripsi();
        }
        if (kodeCuaca < 700) {
            return deskripsi[kodeCuaca / 100];
        } else if (kodeCuaca < 800) {
            return deskripsiAtmosfer[(kodeCuaca % 100)/10];
        } else if (kodeCuaca == 800) {
            return "cerah";
        } else {
            return deskripsi[kodeCuaca / 100];
        }
    }
}

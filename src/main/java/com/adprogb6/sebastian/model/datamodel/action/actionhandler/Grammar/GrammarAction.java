package com.adprogb6.sebastian.model.datamodel.action.actionhandler.Grammar;

import com.adprogb6.sebastian.model.datamodel.action.actionhandler.ActionHandler;
import com.adprogb6.sebastian.service.grammar.GrammarHelper;

public class GrammarAction extends ActionHandler {
    private GrammarHelper grammarHelper;
    private final String failedReponse = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";

    public GrammarAction(String command, String[] content) {
        super(command, content);
    }

    @Override
    public void init() {
        grammarHelper = new GrammarHelper();
    }

    @Override
    public String getTextMessageContent() {
        return getTextMessageResponse();
    }

    @Override
    public String getHeaderContent() {
        return null;
    }

    @Override
    public String getImageUrlContent() {
        return null;
    }

    @Override
    protected void handleInput(String command, String[] content) {
        if (command.equalsIgnoreCase("makna")) {
            handleWordDescriptionCommand(content[0]);
        }else if(command.equalsIgnoreCase("kebakuan")) {
            handleStandardWordCommand(content[0]);
        }else if(command.equalsIgnoreCase("sinonim")) {
            handleThesaurusCommand(content[0], "sinonim");
        }else if(command.equalsIgnoreCase("antonim")) {
            handleThesaurusCommand(content[0], "antonim");
        } else {
            setTextMessageResponse(getFAILEDRESPONSE());
        }
    }

    private void handleThesaurusCommand(String word, String query) {

        String resultData = "" ;
        if(query.equalsIgnoreCase("sinonim")) {
            resultData = grammarHelper.findSynonym(word);
        } else if(query.equalsIgnoreCase("antonim")) {
            resultData = grammarHelper.findAntonym(word);
        }
        StringBuilder responseBuilder = new StringBuilder();
        if (!resultData.equalsIgnoreCase(failedReponse) && !resultData.equalsIgnoreCase("")) {
            responseBuilder.append(String.format("Baik tuan, %s dari kata  %s adalah  \n",query,  word));
        }
        responseBuilder.append(resultData);
        setTextMessageResponse(responseBuilder.toString());
    }

    private void handleWordDescriptionCommand(String word){
        String wordDescription = grammarHelper.findMeaning(word);
        StringBuilder responseBuilder = new StringBuilder();
        if (!wordDescription.equalsIgnoreCase(failedReponse)) {
            responseBuilder.append(String.format("Baik tuan, makna dari kata  %s adalah  \n", word));
        }
        responseBuilder.append(wordDescription);
        setTextMessageResponse(responseBuilder.toString());
    }
    
    private void handleStandardWordCommand(String word){
        boolean isStandard = grammarHelper.isStandard(word);
        StringBuilder responseBuilder = new StringBuilder();
        if(isStandard){
            responseBuilder.append(String.format("Baik tuan, %s adalah kata baku", word));
        }else{
            responseBuilder.append(String.format("Baik tuan, %s bukanlah kata baku", word));
        }
        setTextMessageResponse(responseBuilder.toString());
    }
}
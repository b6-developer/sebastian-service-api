package com.adprogb6.sebastian.model.datamodel.action.actionhandler.help.behaviour;

public class TotalHelperBehaviour  implements HelperBehaviour {
    @Override
    public String getHelperMessageResponse() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Baik tuan, perintah yang tersedia adalah: \n \n");
        stringBuilder.append("Bantuan  tata bahasa:    cari makna, cari kebakuan, cari sinonim, cari antonim \n \n");
        stringBuilder.append("Bantuan butler: tolong pilih, tolong tentukan, tolong kembalikan, tolong laporkan-cuaca, " +
                "tolong laporkan-suhu, tolong laporkan-kelembapan\n \n");
        stringBuilder.append("Bantuan musik: berikan top-artist [jumlah artist], berikan top-songs [jumlah lagu], berikan informasi, berikan lirik-lagu, berikan artis\n \n");
        stringBuilder.append("Bantuan berita: tampilkan top-headlines, tampilkan berita,tampilkan kategori \n \n");
        stringBuilder.append("Secara umum perintah yang bisa tuan lakukan adalah " +
                "Sebastian, [identifier kelompok fitur] [perintah spesifik] [parameter]");
        return stringBuilder.toString();
    }
    public static void main(String[] args) {
        TotalHelperBehaviour totalHelperBehaviour = new TotalHelperBehaviour();


    }
}

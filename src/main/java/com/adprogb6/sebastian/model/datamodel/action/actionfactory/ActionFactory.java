package com.adprogb6.sebastian.model.datamodel.action.actionfactory;

import com.adprogb6.sebastian.model.datamodel.action.actionhandler.ActionHandler;
import com.adprogb6.sebastian.model.datamodel.action.actionhandler.Butler.ButlerAction;
import com.adprogb6.sebastian.model.datamodel.action.actionhandler.Grammar.GrammarAction;
import com.adprogb6.sebastian.model.datamodel.action.actionhandler.help.HelpAction;

public class ActionFactory {


    public ActionHandler createAction(String identifier, String command, String[] content) {
        ActionHandler actionHandler = null;
        if (identifier.equalsIgnoreCase("cari")) {
            actionHandler = new GrammarAction(command, content);
        } else if (identifier.equalsIgnoreCase("tolong")) {
            actionHandler = new ButlerAction(command, content);
        }else {
            actionHandler = new HelpAction(command, content);
        }
        return actionHandler;

    }

}

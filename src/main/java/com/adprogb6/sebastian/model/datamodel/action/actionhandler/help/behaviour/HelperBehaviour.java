package com.adprogb6.sebastian.model.datamodel.action.actionhandler.help.behaviour;

public interface HelperBehaviour {

    public  String getHelperMessageResponse();
}

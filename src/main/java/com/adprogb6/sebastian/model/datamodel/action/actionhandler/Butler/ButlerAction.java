package com.adprogb6.sebastian.model.datamodel.action.actionhandler.Butler;

import com.adprogb6.sebastian.model.datamodel.action.actionhandler.ActionHandler;
import com.adprogb6.sebastian.service.butler.ButlerHelper;
import com.adprogb6.sebastian.service.butler.WeatherHelper;
import com.adprogb6.sebastian.service.grammar.GrammarUtil;
import org.springframework.web.client.HttpClientErrorException;
import com.adprogb6.sebastian.model.datamodel.cuaca.Cuaca;

public class ButlerAction extends ActionHandler {
    ButlerHelper butlerHelper;
    WeatherHelper weatherHelper;
    public ButlerAction(String command, String[] content) {
        super(command, content);
    }

    @Override
    public String getTextMessageContent() {
        return getTextMessageResponse();
    }

    @Override
    public String getHeaderContent() {
        return null;
    }

    @Override
    public String getImageUrlContent() {
        return null;
    }

    @Override
    protected void handleInput(String command, String[] content) {
        if (command.equalsIgnoreCase("laporkan-cuaca")) {
            handleWeatherCommand(content[0]);
        } else if (command.equalsIgnoreCase("laporkan-suhu")) {
            handleTempCommand(content[0]);
        } else if (command.equalsIgnoreCase("laporkan-kelembapan") || command.equalsIgnoreCase("laporkan-kelembaban")) {
            handleHumidityCommand(content[0]);
        } else if (command.equalsIgnoreCase("tentukan")) {
            setTextMessageResponse(butlerHelper.getAnswer(content));
        } else if (command.equalsIgnoreCase("pilih")) {
            handleChoiceCommand(content);
        } else if (command.equalsIgnoreCase("roll")) {
            handleRollCommand(content[0]);
        }
    }

    private void handleRollCommand(String s) {
        StringBuilder responseBuilder = new StringBuilder();
        try {
            responseBuilder.append("Baik tuan, saya mendapatkan ");
            responseBuilder.append(butlerHelper.getRandomNumber(Integer.parseInt(s)));
            setTextMessageResponse(responseBuilder.toString());
        } catch (NumberFormatException e) {
            setTextMessageResponse("Maaf tuan, saya tidak dapat melakukan perintah tuan");
        }
    }

    private void handleChoiceCommand(String[] content) {
        StringBuilder responseBuilder = new StringBuilder();
        responseBuilder.append("Saya memilih ");
        responseBuilder.append(butlerHelper.choose(content));
        responseBuilder.append("tuan");
        setTextMessageResponse(responseBuilder.toString());
    }

    @Override
    public void init() {
        butlerHelper = ButlerHelper.getInstance();
        weatherHelper = WeatherHelper.getInstance();
    }

    private void handleWeatherCommand(String kota) {
        StringBuilder responseBuilder = new StringBuilder();
        try {
            Cuaca cuaca = weatherHelper.getCuaca(kota);
            kota = GrammarUtil.capitalize(kota);
            responseBuilder.append(String.format("Baik tuan, cuaca di %s  adalah ",kota));
            responseBuilder.append(cuaca.getDescription());
        } catch (HttpClientErrorException e) {
            responseBuilder.append("Maaf tuan, saya tidak bisa menemukan cuaca untuk kota tersebut");
        }
        setTextMessageResponse(responseBuilder.toString());
    }

    private void handleTempCommand(String kota) {
        StringBuilder responseBuilder = new StringBuilder();
        try {
            Cuaca cuaca = weatherHelper.getCuaca(kota);
            kota = GrammarUtil.capitalize(kota);
            responseBuilder.append(String.format("Baik tuan, suhu di %s adalah ",kota));
            responseBuilder.append(cuaca.getTemp() + "° C");
        } catch (HttpClientErrorException e) {
            responseBuilder.append("Maaf tuan, saya tidak bisa menemukan suhu untuk kota tersebut");
        }
        setTextMessageResponse(responseBuilder.toString());
    }

    private void handleHumidityCommand(String kota) {
        StringBuilder responseBuilder = new StringBuilder();
        try {
            Cuaca cuaca = weatherHelper.getCuaca(kota);
            kota = GrammarUtil.capitalize(kota);
            responseBuilder.append(String.format("Baik tuan, kelembapan di %s adalah ",kota));
            responseBuilder.append(cuaca.getHumidity());
            responseBuilder.append("%");
        } catch (HttpClientErrorException e) {
            responseBuilder.append("Maaf tuan, saya tidak bisa menemukan kelembapan untuk kota tersebut");
        }
        setTextMessageResponse(responseBuilder.toString());
    }
}

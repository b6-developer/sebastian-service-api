package com.adprogb6.sebastian.model.datamodel.action.actionhandler;

public abstract class ActionHandler {
    private String command;
    private String[] content;

    private String textMessageResponse;


    private static final String FAILEDRESPONSE = "Maafkan saya tuan, saya tidak memahami" +
            "yang tuan katakan";

    protected ActionHandler(String command, String[] content) {
        this.command = command;
        this.content = content;
        init();
        handleInput(command, content);
    }
    public abstract String getTextMessageContent();

    public abstract String getHeaderContent();

    public abstract String getImageUrlContent();

    public abstract void init();

    protected abstract void handleInput(String command, String[] content);

    protected String getCommand() {
        return command;
    }

    protected void setCommand(String command) {
        this.command = command;
    }

    protected String[] getContent() {
        return content;
    }

    protected void setContent(String[] content) {
        this.content = content;
    }


    protected void setTextMessageResponse(String textMessageResponse) {
        this.textMessageResponse = textMessageResponse;
    }


    protected String getFAILEDRESPONSE() {
        return FAILEDRESPONSE;
    }

    protected String getTextMessageResponse() {
        return textMessageResponse;
    }
}

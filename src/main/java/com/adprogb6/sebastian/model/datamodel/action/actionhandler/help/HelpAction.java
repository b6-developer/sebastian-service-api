package com.adprogb6.sebastian.model.datamodel.action.actionhandler.help;

import com.adprogb6.sebastian.model.datamodel.action.actionhandler.ActionHandler;
import com.adprogb6.sebastian.model.datamodel.action.actionhandler.help.behaviour.HelperBehaviour;
import com.adprogb6.sebastian.service.help.HelperActionFactory;

public class HelpAction extends ActionHandler {
    private HelperActionFactory helperActionFactory;
    private HelperBehaviour helperBehaviour;
    public HelpAction(String command, String[] content) {
        super(command, content);
    }

    @Override
    protected void handleInput(String command, String[] content) {
        helperBehaviour = helperActionFactory.createHelperAction(command, content);
    }

    @Override
    public void init() {
        helperActionFactory = new HelperActionFactory();
    }

    @Override
    public String getTextMessageContent() {
        return helperBehaviour.getHelperMessageResponse();
    }

    @Override
    public String getHeaderContent() {
        return null;
    }

    @Override
    public String getImageUrlContent() {
        return null;
    }
}

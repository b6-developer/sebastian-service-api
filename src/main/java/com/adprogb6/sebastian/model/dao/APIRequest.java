package com.adprogb6.sebastian.model.dao;

public class APIRequest {

    private String request;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}

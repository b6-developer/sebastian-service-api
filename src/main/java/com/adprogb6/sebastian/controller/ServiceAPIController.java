package com.adprogb6.sebastian.controller;

import com.adprogb6.sebastian.model.dao.APIRequest;
import com.adprogb6.sebastian.service.Inputprocessing.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ServiceAPIController {

    @Autowired
    private ResponseService responseService;

    @PostMapping("/service-response")
    public ResponseEntity<String> getServiceAPIResponse(@RequestBody APIRequest request) {
        String response = responseService.responseToUser(request.getRequest());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}

package com.adprogb6.sebastian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SebastianApplication extends SpringBootServletInitializer {




    public static void main(String[] args) { SpringApplication.run(SebastianApplication.class, args); }

}

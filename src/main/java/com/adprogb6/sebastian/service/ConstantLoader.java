package com.adprogb6.sebastian.service;



public class ConstantLoader {
   private static ConstantStrategy constantStrategy = new DeploymentConstantStrategy();



    public static String getIndonesiaGrammarUrl() {
       return constantStrategy.getIndonesiaGrammarUrl();

    }

    public static String getWeatherAPIKey() {
       return constantStrategy.getWeatherAPIKey();
    }
}

package com.adprogb6.sebastian.service;

import io.github.cdimascio.dotenv.Dotenv;

public class DevelopmentConstantStrategy implements ConstantStrategy {
    Dotenv loader = Dotenv.load();

    @Override
    public String getWeatherAPIKey() {
        return loader.get("OPENWEATHER_API_KEY");
    }

    @Override
    public String getIndonesiaGrammarUrl() {
        return loader.get("GRAMMAR_URL_Id");
    }
}

package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import com.adprogb6.sebastian.service.grammar.strategy.GrammarBehaviour;


public class IndonesianGrammarBehaviour extends GrammarBehaviour {
    public IndonesianGrammarBehaviour() {
        super(new IndonesianSynonymHelperMicroservice(), new IndonesianAntonymHelper(),
                new IndonesianWordDescriptionHelper());
    }
}
package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import com.adprogb6.sebastian.service.grammar.strategy.AntonymHelper;

public class IndonesianAntonymHelper implements AntonymHelper {
    private IndonesianSynonymAntonymResponseCreator responseCreator;


    public IndonesianAntonymHelper() {
        this.responseCreator =  new IndonesianSynonymAntonymResponseCreator("antonim");
    }

    @Override
    public String findAntonym(String word) {
        if (word.equalsIgnoreCase("")) {
            return IndonesianSynonymAntonymResponseCreator.FAILEDRESPONSE;
        }
        return responseCreator.createResponse(word);
    }




}

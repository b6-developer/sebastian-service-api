package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import com.adprogb6.sebastian.model.datamodel.GrammarDataResponse;
import com.adprogb6.sebastian.service.ConstantLoader;
import org.springframework.web.client.RestTemplate;

public class IndonesianSynonymAntonymResponseCreator {
    RestTemplate restTemplate = new RestTemplate();
    private String baseTargetUrl = ConstantLoader.getIndonesiaGrammarUrl();
    protected static final String FAILEDRESPONSE = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";

    public IndonesianSynonymAntonymResponseCreator(String type) {
        this.baseTargetUrl+=type+"/";
    }

    public String createResponse(String words) {
        GrammarDataResponse antonymResponse =restTemplate.getForObject(baseTargetUrl +words, GrammarDataResponse.class);
        if (antonymResponse!=null && antonymResponse.getStatus()==200) {
            return antonymResponse.getResult();
        } else {
            return FAILEDRESPONSE;
        }
    }
}

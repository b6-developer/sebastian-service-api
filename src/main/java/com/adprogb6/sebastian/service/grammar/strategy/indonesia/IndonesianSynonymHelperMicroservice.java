package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import com.adprogb6.sebastian.service.grammar.strategy.SynonymHelper;

public class IndonesianSynonymHelperMicroservice implements SynonymHelper {
    private IndonesianSynonymAntonymResponseCreator responseCreator;

    public IndonesianSynonymHelperMicroservice() {
        this.responseCreator = new IndonesianSynonymAntonymResponseCreator("sinonim");
    }

    @Override
    public String findSynonym(String words) {
        if (words.equalsIgnoreCase("")) {
            return IndonesianSynonymAntonymResponseCreator.FAILEDRESPONSE;
        }
        return responseCreator.createResponse(words);
    }


}

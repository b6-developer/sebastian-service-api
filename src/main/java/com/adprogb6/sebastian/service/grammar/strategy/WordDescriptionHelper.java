package com.adprogb6.sebastian.service.grammar.strategy;

public interface WordDescriptionHelper {
    public String findMeaning(String word);
    public boolean isStandard(String word);
}

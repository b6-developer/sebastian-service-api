package com.adprogb6.sebastian.service.grammar.strategy;

public interface AntonymHelper {
    public String findAntonym(String word);
}

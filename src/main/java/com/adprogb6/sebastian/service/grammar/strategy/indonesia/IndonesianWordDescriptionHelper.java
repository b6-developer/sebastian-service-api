package com.adprogb6.sebastian.service.grammar.strategy.indonesia;

import java.io.IOException;

import com.adprogb6.sebastian.service.grammar.strategy.WordDescriptionHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


public class IndonesianWordDescriptionHelper implements WordDescriptionHelper {
    private static final String FAILED_RESPONSE = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";

    public String findMeaning(String word){
        String responseToInput = "";
        try {
            Document doc = Jsoup.connect("https://kbbi.web.id/" + word).get();
            Element table = doc.select("div#d1").first();
            String resultWord = table.ownText();

            if(resultWord.equals("")){
                responseToInput = FAILED_RESPONSE;
            }else{
                responseToInput = resultWord;
            }
        } catch (IOException e){
            responseToInput = FAILED_RESPONSE;
        }
        return responseToInput;
    }

    public boolean isStandard(String word){
        return !findMeaning(word).equals(FAILED_RESPONSE);
    }
}

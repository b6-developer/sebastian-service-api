package com.adprogb6.sebastian.service.grammar.strategy;



public abstract class GrammarBehaviour {
    private SynonymHelper synonymHelper;
    private AntonymHelper antonymHelper;
    private WordDescriptionHelper wordDescriptionHelper;

    protected GrammarBehaviour(SynonymHelper synonymHelper, AntonymHelper antonymHelper,
                            WordDescriptionHelper wordDescriptionHelper) {
        this.synonymHelper = synonymHelper;
        this.antonymHelper = antonymHelper;
        this.wordDescriptionHelper = wordDescriptionHelper;
    }

    public String findSynonym(String word) {
        return synonymHelper.findSynonym(word);
    }
    public  String findAntonym(String word) {
        return antonymHelper.findAntonym(word);
    }
    public String findMeaning(String word){
        return wordDescriptionHelper.findMeaning(word);
    }
    public boolean isStandard(String word){ return wordDescriptionHelper.isStandard(word); }
}

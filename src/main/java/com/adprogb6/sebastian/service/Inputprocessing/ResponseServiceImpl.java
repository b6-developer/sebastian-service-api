package com.adprogb6.sebastian.service.Inputprocessing;

import com.adprogb6.sebastian.model.datamodel.action.actionfactory.ActionFactory;
import com.adprogb6.sebastian.model.datamodel.action.actionhandler.ActionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResponseServiceImpl implements ResponseService {

    private ActionFactory actionFactory = new ActionFactory();

    @Autowired
    private InputService inputService;

    @Override
    public String responseToUser(String userMessage) {
        inputService.takeInput(userMessage);
        if (inputService.isACommand()) {
            return getMessageResponse();
        }
        else {
            return null;
        }
    }

    private String getMessageResponse() {
        String response;
        if (inputService.isValidInput()) {
            ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                    inputService.getCommand(), inputService.getContent());
            response = actionHandler.getTextMessageContent();
        } else {
            response = inputService.getCommand();
        }
        return response;
    }
}

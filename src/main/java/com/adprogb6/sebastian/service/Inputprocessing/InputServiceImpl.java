package com.adprogb6.sebastian.service.Inputprocessing;

import org.springframework.stereotype.Service;


@Service
public class InputServiceImpl implements InputService {
    private String headInput;
    private String identifier;
    private String command;
    private String[] content;
    private boolean isCommand = true;
    private boolean isValid = true;


    @Override
    public void takeInput(String userInput) {
        initializeClass();
        String[] userInputWordsArray = userInput.split(" ");
        validateInputACommand(userInputWordsArray);
        processInput(userInputWordsArray);
    }

    private void processInput(String[] userInputWordsArray) {
        if (isCommand) {
            validateIdentifier(userInputWordsArray, findStartingHeadCommandTextIndex(userInputWordsArray));
            validateCommandTextPart(userInputWordsArray, findStartingHeadCommandTextIndex(userInputWordsArray));
            validateContentFromInput(userInputWordsArray, findStartingHeadCommandTextIndex(userInputWordsArray));
        } else {
            setFalseInputArgument();
        }
    }

    private void getCommandInput(String[] userInputWordsArray, int startingCommandIndex) {
        command = userInputWordsArray[startingCommandIndex];
    }

    private void validateCommandTextPart(String[] userInputWordsArray, int startingIndex) {
        try {
            getCommandInput(userInputWordsArray, getCommandPartIndex(startingIndex));
        }catch (ArrayIndexOutOfBoundsException e) {
            isValid = false;
        }
    }

    private int getCommandPartIndex(int startingIndex) {
        return 2+startingIndex;
    }

    private void getContentInput(String[] userInputWordsArray, int startingContent) {
        StringBuilder contentTextBuider = new StringBuilder();
        for (int x = startingContent; x<userInputWordsArray.length; x++) {
            contentTextBuider.append(userInputWordsArray[x]);
            contentTextBuider.append(" ");
        }
        String contentInString = contentTextBuider.toString();
        content = contentInString.split(" ");
    }

    private void validateContentFromInput(String[] userInputWordsArray, int startingIndex) {
        getContentInput(userInputWordsArray,getContentPartIndex(startingIndex));
        for (int x = 0; x < content.length;x++) {
            if (content[x].equalsIgnoreCase("")) {
                isValid = false;
            }
        }
    }

    private int getContentPartIndex(int startingIndex) {
        return 3+startingIndex;
    }


    private void validateIdentifier(String[] userInputWordsArray, int startingIndex) {
        try {
            findIdentifierText(getIdentifierPartIndex(startingIndex), userInputWordsArray);
        }catch (ArrayIndexOutOfBoundsException e) {
            isValid = false;
        }
    }

    private int getIdentifierPartIndex(int startingIndex) {
        return 1+startingIndex;
    }





    private int findStartingHeadCommandTextIndex(String[] userInputWordsArray) {
        int searchIndex = 0;
        for (int x = 0; x < userInputWordsArray.length; x++ ) {
            if (userInputWordsArray[x].equalsIgnoreCase("sebastian,")) {
                headInput = userInputWordsArray[x];
                searchIndex = x;
            }
        }
        return searchIndex;
    }

    private void validateInputACommand(String[] userInputWordsArray) {
        for (int x = 0; x < userInputWordsArray.length; x++ ) {
            if (userInputWordsArray[x].equalsIgnoreCase("sebastian,")) {
                return;
            }
        }
        isCommand = false;
    }

    private void findIdentifierText(int startingIndex, String[] userInputWordsArray) {
        if (!userInputWordsArray[startingIndex].equalsIgnoreCase("cari") &&
                !userInputWordsArray[startingIndex].equalsIgnoreCase("tolong")&&
                !userInputWordsArray[startingIndex].equalsIgnoreCase("berikan")&&
                !userInputWordsArray[startingIndex].equalsIgnoreCase("tampilkan")&&
                !userInputWordsArray[startingIndex].equalsIgnoreCase("help")) {
            isValid = false;
        }
        identifier= userInputWordsArray[startingIndex];
    }

    private String notValidCommand() {
        return "maaf, saya tidak bisa memahami yang tuan katakan";
    }

    @Override
    public boolean isValidInput() {
        return isValid;
    }

    private void initializeClass() {
        isValid = true;
        isCommand = true;
    }

    private void  setFalseInputArgument() {
        isCommand = false;
        isValid = false;
    }

    @Override
    public String getHeadInput() {
        return headInput;
    }

    @Override
    public String getIdentifier() {
        if(!isValidInput()) {
            return notValidCommand();
        }
        return identifier;
    }

    @Override
    public String getCommand() {
        if(!isValidInput()) {
            return notValidCommand();
        }
        return command;
    }

    @Override
    public String[] getContent() {
        return content;
    }

    @Override
    public boolean isACommand() {
        return isCommand;
    }


}
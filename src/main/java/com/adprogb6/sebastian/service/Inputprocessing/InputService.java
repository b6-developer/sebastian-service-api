package com.adprogb6.sebastian.service.Inputprocessing;

public interface InputService {
    public void takeInput(String userInput);
    public String getHeadInput();
    public String getIdentifier();
    public String getCommand();
    public String[] getContent();
    public boolean isValidInput();
    public boolean isACommand();
}

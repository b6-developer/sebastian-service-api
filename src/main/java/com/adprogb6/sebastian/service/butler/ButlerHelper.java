package com.adprogb6.sebastian.service.butler;

import java.security.SecureRandom;

import com.adprogb6.sebastian.service.grammar.GrammarUtil;

public class ButlerHelper {
    private static SecureRandom generator = new SecureRandom();
    private static ButlerHelper butlerHelper = new ButlerHelper();



    private ButlerHelper() {

    }
    public String choose(String[] choices) {
        String choiceAsString = GrammarUtil.convertToString(choices);
        if (isContainBridgeKeyword(choices)) {
            String[] parameterChoices = choiceAsString.split("atau");
            return parameterChoices[generator.nextInt(parameterChoices.length)];
        } else {
            return GrammarUtil.convertToString(choices);
        }
    }

    private boolean isContainBridgeKeyword(String[] choices) {
        boolean isFound = false;
        int index = 0;
        while (!(isFound ) && index < choices.length) {
            if (choices[index].equalsIgnoreCase("atau")) {
                isFound = true;
            }
            index++;
        }
        return isFound;
    }

    public String getAnswer(String[] userInput) {
        String userResponse = GrammarUtil.convertToString(userInput);
        if (generator.nextInt(2) == 0) {
            return  userResponse+" \n " + " Tidak tuan";
        } else {
            return userResponse+ " \n"+ " Ya tuan";
        }
    }

    public int getRandomNumber(int range) {
        return 1+generator.nextInt(range);
    }

    public static ButlerHelper getInstance() {
        return butlerHelper;
    }

}

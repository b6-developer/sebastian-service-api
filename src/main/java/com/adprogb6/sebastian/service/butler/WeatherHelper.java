package com.adprogb6.sebastian.service.butler;

import com.adprogb6.sebastian.model.datamodel.cuaca.Cuaca;
import com.adprogb6.sebastian.service.ConstantLoader;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class WeatherHelper {
    private static String OPENWEATHER_API_KEY= ConstantLoader.getWeatherAPIKey();
    private static WeatherHelper weatherHelper = new WeatherHelper();

    private WeatherHelper() {

    }

    public static WeatherHelper getInstance() {
        return weatherHelper;
    }

    public Cuaca getCuaca(String kota) {
        try {
            kota = kota.toLowerCase();
            RestTemplate rest = new RestTemplate();
            Cuaca cuaca = rest.getForObject("http://api.openweathermap.org/data/2.5/weather?q=" +
                    kota +
                    "&units=metric&appid=" + OPENWEATHER_API_KEY, Cuaca.class);
            return cuaca;
        } catch (HttpClientErrorException e) {
            throw e;
        }
    }
}

package com.adprogb6.sebastian.service;

public interface ConstantStrategy {
    public String getIndonesiaGrammarUrl();
    public String getWeatherAPIKey();
}
